FROM ubuntu

RUN apt-get update && apt-get -y install libexpat1 libexpat1-dev wget iputils-ping

ENV CONSUL_TEMPL_VER 0.15.0
RUN wget https://releases.hashicorp.com/consul-template/${CONSUL_TEMPL_VER}/consul-template_${CONSUL_TEMPL_VER}_linux_amd64.zip && \
    gunzip -S .zip consul-template_${CONSUL_TEMPL_VER}_linux_amd64.zip && \
    mv consul-template_${CONSUL_TEMPL_VER}_linux_amd64 /usr/local/bin/consul-template && \
    chmod +x /usr/local/bin/consul-template

WORKDIR /app
ADD gossipDemo /app/
ADD templates/ /app/

EXPOSE 8080

CMD /usr/local/bin/consul-template -consul 172.33.4.1:8500 \
    -template "/app/gossip.toml.templ:/app/gossip.toml:pkill gossipDemo;sleep 5;/app/gossipDemo $GOSSIP_PORT $KNOWN_HOSTS &" \
    -retry 30s