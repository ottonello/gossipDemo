build:
	./build.sh
	docker build -t gossip .

create-net:
	docker network create --driver bridge --subnet 172.33.0.0/16 gossip

install: create-net
	pip install netaddr

start: build
	./start_dockers.py 2> log.txt

consul-start:
	docker run --name consul --net gossip --ip 172.33.4.1 -p 8400:8400 -p 8500:8500 -p 8600:53/udp -h node1 progrium/consul -server -advertise 172.33.4.1 -bootstrap -ui-dir /ui

test: 
	curl -X POST http://localhost:3000/ -d "msg=test"

kill:
	docker ps -q -f ancestor=gossip | xargs -n 1 --max-procs=0 docker kill

rmi:
	docker images|awk '{print $3}'| xargs docker rmi
