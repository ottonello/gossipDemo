## Gossip implementation in Go

# Requirements:

- golang 1.7.5 https://golang.org/dl/
- docker https://www.docker.com/products/overview
- python3 + pip

Configuration:
Configuration of each gossip node is taken from Consul. Used keys are:
- `/gossip/delay`: delay between gossip cycles.
- `/gossip/fanout`: amount of other nodes the message is shared with on each cycle.
- `/gossip/cycles`: amount of cycles during which the message is remembered.

Run:
- `pip install -r requirements.txt` to get Python deps
- `go get` to download all deps.
- `make build` to build the docker image.
- `make create-net` to create the docker overlay network to be used by the containers.
- `make consul-start` to start the consul instance where configuration will be read from. The ui for this instance is mapped to local port 8500.
- `make start` to start.
- `make kill` to kill all docker containers.
- `make test` to inject start gossipping a message.