package eventLog

import (
	"log"
	"time"
	"encoding/json"
	"fmt"
)

type EventLog struct {
	logId string
}

type Event struct {
	Msg      string    `json:"msg,omitempty"`
	EvtType  string    `json:"type,omitempty"`
	Instance string    `json:"instance,omitempty"`
	LogId    string    `json:"logId,omitempty"`
	Time     time.Time `json:"time,omitempty"`
	Dest     string    `json:"dest,omitempty"`
	Detail   string    `json:"detail,omitempty"`
}

func NewEventLog(logId string) *EventLog {
	log.SetFlags(0)

	return &EventLog{logId}
}

func (evtLog EventLog) StartedNode(ip string, fanout int, cycles int, dropProbability float32) {
	var event = &Event{"", "started", ip, evtLog.logId, time.Now(), "", fmt.Sprintf("f=%d, c=%d p=%f", fanout, cycles, dropProbability)}
	store(event)
}

func (evtLog EventLog) ReceivedMessage(msgAsString string, ip string) {
	var event = &Event{msgAsString, "received", ip, evtLog.logId, time.Now(), "", ""}
	store(event)
}

func (evtLog EventLog) ReceivedKnownMessage(msgAsString string, ip string) {
	var event = &Event{msgAsString, "known", ip, evtLog.logId, time.Now(), "", ""}
	store(event)
}

func (evtLog EventLog) SentMessage(msgAsString string, ip string, destIp string) {
	var event = &Event{msgAsString, "sent", ip, evtLog.logId, time.Now(), destIp, ""}
	store(event)
}

func (evtLog EventLog) RemoveMessage(msgAsString string, ip string) {
	var event = &Event{msgAsString, "removed", ip, evtLog.logId, time.Now(), "", ""}
	store(event)
}

func (evtLog EventLog) ErrorSending(msgAsString string, ip string, destIp string, err error) {
	var event = &Event{msgAsString, "error", ip, evtLog.logId, time.Now(), destIp, err.Error()}
	store(event)
}

func store(event *Event) {
	chars, _ := json.Marshal(event)
	log.Println(string(chars))
}

