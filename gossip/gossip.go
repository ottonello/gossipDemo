package gossip

import (
	"fmt"
	"net"
	"os"
	"time"
)

// SelectPeers selects a number of peer addresses to which the gossip shall be sent
type SelectPeers func(fanout uint64) []string

// GetDataToSend returns the data that should be gossiped as payload
type GetDataToSend func() []byte

// ProcessData is called whenever a gossip message is received
type ProcessData func([]byte)

// MessageSent is called whenever a gossip message is sent to a node
type MessageSent func(string, string)

// MessageSent is called when some error happened while sending a message
type ErrorSending func(string, string, error)

// Gossip contains all information relative
type Gossip struct {
	config      *Config
	selectPeers SelectPeers
	getData     GetDataToSend
	processData ProcessData
	messageSent MessageSent
	errorSending ErrorSending
}

// Config contains all configurable parameters of a gossip instance
type Config struct {
	LocalIP string
	Port    string
	Delay   time.Duration
	Fanout  uint64
}

// NewConfig builds a new gossip configuration
func NewConfig(localIP string, port string, delay time.Duration, fanout uint64) *Config {
	return &Config{localIP, port, delay, fanout}
}

// NewGossip builds a new gossip instance with passed configuration
func NewGossip(config *Config, selectFn SelectPeers, dataFn GetDataToSend, processFn ProcessData, messageSent MessageSent, errorSending ErrorSending) *Gossip {
	return &Gossip{config, selectFn, dataFn, processFn, messageSent, errorSending}
}

// Start both gossip processes, a loop for sending messages with the given delay,
// and another loop which awaits for incoming messages.
func (gossip *Gossip) Start() {
	go gossip.receiveLoop()
	go gossip.sendLoop()
}

func (gossip *Gossip) sendLoop() {
	for {
		time.Sleep(gossip.config.Delay)
		update := gossip.getData()

		if update != nil {
			targets := gossip.selectPeers(gossip.config.Fanout)

			for _, host := range targets {
				go gossip.send(host, update)
			}
		}
	}
}

// Send a single update to a single host
func (gossip *Gossip) send(host string, update []byte) {
	peerAddr, err := net.ResolveUDPAddr("udp", host + ":" + gossip.config.Port)
	if err != nil {
		// TODO fix all error handling here, log something better here:
		fmt.Println("Error resolving address")
	}

	//localAddr, err := net.ResolveUDPAddr("udp", gossip.config.localIP+":0")
	//checkError(err)

	conn, err := net.DialUDP("udp", nil, peerAddr)
	checkError(err)

	defer conn.Close()

	_, err = conn.Write(update)
	if err != nil {
		//fmt.Println("Error sending update", err)
		gossip.errorSending(string(update), host, err)
	}
	gossip.messageSent(string(update), host)
}

func (gossip *Gossip) receiveLoop() {
	serverAddr, err := net.ResolveUDPAddr("udp", ":" + gossip.config.Port)
	checkError(err)
	serverConn, err := net.ListenUDP("udp", serverAddr)
	checkError(err)
	defer serverConn.Close()

	buf := make([]byte, 1024)

	for {
		n, _, err := serverConn.ReadFromUDP(buf)

		gossip.processData(buf[0:n])

		if err != nil {
			fmt.Print("Error processing update", err)
		}
	}
}

func checkError(err error) {
	if err != nil {
		os.Exit(0)
	}
}
