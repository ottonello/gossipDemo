package main

import (
	"gopkg.in/alecthomas/kingpin.v2"
	"fmt"
	"os"
	"bufio"
	"gitlab.com/ottonello/gossipDemo/eventLog"
	"encoding/json"
	"time"
)

var (
	name = kingpin.Arg("file", "Path to the log file.").Required().String()
)

type NodeStats struct {
	started  time.Time
	sent     int
	received int
}

type TotalStats struct {
	msg           string
	totalNodes    int
	sent          int
	received      int
	known         int
	removed       int
	error	      int
	firstReceived time.Time
	firstSent     time.Time
	lastRemoved   time.Time
	receivedOn    map[*string]bool
}

func main() {
	kingpin.Parse()
	file, err := os.Open(*name)
	if (err != nil) {
		fmt.Printf("Could not open file, %v\n", err)
		os.Exit(1)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	nodes := map[string]*NodeStats{}
	totalStats := &TotalStats{receivedOn: make(map[*string]bool)}
	l := 0
	var firstInterval time.Time
	// var lastInterval time.Time

	for scanner.Scan() {
		line := scanner.Text()
		l++

		evt := &eventLog.Event{}
		json.Unmarshal([]byte(line), evt)

		ip := evt.Instance
		if totalStats.msg == "" {
			totalStats.msg = evt.Msg
		}
		if evt.Msg != "" && totalStats.msg != evt.Msg {
			panic("Message other than expected!")
		}

		nodeStats := nodes[ip]
		if(nodeStats == nil){
			nodes[ip] = &NodeStats{evt.Time, 0, 0}
		}

		switch evt.EvtType {
		case "sent":
			nodeStats.sent++
			totalStats.sent++
			if totalStats.firstSent.IsZero() {
				totalStats.firstSent = evt.Time
			}
		case "received":
			if(nodeStats == nil){
				continue
			}
			nodeStats.received++
			totalStats.received++
			if totalStats.firstReceived.IsZero() {
				totalStats.firstReceived = evt.Time
			}
			if !totalStats.receivedOn[&evt.Instance] {
				totalStats.receivedOn[&evt.Instance] = true
			}
			if(firstInterval.IsZero()){
				firstInterval = evt.Time
			}
			// if(lastInterval.IsZero() || evt.Time.After(lastInterval.Add(500*time.Millisecond))){
			fmt.Printf("%.3f,%v\n", evt.Time.Sub(firstInterval).Seconds(), totalStats.received)
			// lastInterval = evt.Time
			// }
		case "known":
			totalStats.known++
		case "removed":
			totalStats.removed++
			if totalStats.lastRemoved.Before(evt.Time) {
				totalStats.lastRemoved = evt.Time
			}
		case "started":
			totalStats.totalNodes++
		case "error":
			totalStats.error++
		default:
			panic(fmt.Sprintf("unknown event type at line %d: '%+v' %+v", l, line, evt))
		}

	}
	fmt.Printf("%.3f,%v\n", totalStats.lastRemoved.Sub(firstInterval).Seconds(), totalStats.received)
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading from file:", err)
		os.Exit(1)
	}
	fmt.Printf("totalNodes: %v\n", totalStats.totalNodes)
	fmt.Printf("sent: %v\n", totalStats.sent)
	fmt.Printf("received: %v\n", totalStats.received)
	fmt.Printf("errors: %v\n", totalStats.error)
	fmt.Printf("known: %v\n", totalStats.known)
	fmt.Printf("removed: %v\n", totalStats.removed)
	fmt.Printf("firstSent: %v\n", totalStats.firstSent)
	fmt.Printf("firstReceived: %v\n", totalStats.firstReceived)
	fmt.Printf("lastRemoved: %v\n", totalStats.lastRemoved)
	fmt.Printf("total duration: %v\n", totalStats.lastRemoved.Sub(totalStats.firstReceived))

}

