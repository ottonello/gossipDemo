package main

import (
	"errors"
	"log"
	"math/rand"
	"net"
	"net/http"
	"os"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/Workiva/go-datastructures/queue"
	"github.com/Workiva/go-datastructures/set"
	"github.com/go-martini/martini"
	l "gitlab.com/ottonello/gossipDemo/eventLog"
	"gitlab.com/ottonello/gossipDemo/gossip"
)

type Application struct {
	localIp    		string
	knownHosts 		[]string
	pending    		*queue.Queue
	received   		*set.Set
	maxCycles 		int
	eventLog   		*l.EventLog
	coin 			bool
	dropProbability	float32
}

type Update struct {
	message []byte
	cycles  int
}

type GossipConfig struct {
	Delay	string
	Fanout	int
	Cycles 	int
	DropProbability		float32
}

func ReadConfig() GossipConfig {
	var configfile = "gossip.toml"
	_, err := os.Stat(configfile)
	if err != nil {
		log.Fatal("Config file is missing: ", configfile)
	}

	var config GossipConfig
	if _, err := toml.DecodeFile(configfile, &config); err != nil {
		log.Fatal(err)
	}
	return config
}

func main() {
	localIp, _ := getIp()
	port := os.Args[1]

	var conf GossipConfig = ReadConfig()
	delay, _ := time.ParseDuration(conf.Delay)

	testId := "test"
	hosts := removeIp(os.Args[2:], localIp)

	rand.Seed(time.Now().UnixNano())

	app := Application{localIp, hosts, queue.New(64), set.New(), conf.Cycles, l.NewEventLog(testId), false, conf.DropProbability}
	config := gossip.NewConfig(localIp, port, delay, uint64(conf.Fanout))
	gos := gossip.NewGossip(config, app.selectPeers, app.getData, app.processData, app.messageSent, app.errorSending)
	gos.Start()

	app.eventLog.StartedNode(localIp, conf.Fanout, conf.Cycles, conf.DropProbability);

	m := martini.Classic()
	m.Post("/", func(req *http.Request) string {
		msg := req.FormValue("msg")
		app.addMessage(msg)

		return "OK"
	})
	m.Delete("/", func(req *http.Request) string {
		log.Println("Cleaning up database")
		app.pending = queue.New(64)

		return "OK"
	})
	m.Run()
}

func (app *Application) selectPeers(n uint64) []string {
	// choose random peer
	peers := make([]string, n)
	for i := uint64(0); i < n; i++ {
		randomIndex := rand.Intn(len(app.knownHosts))
		peers[i] = app.knownHosts[randomIndex]
	}

	return peers
}

func (app *Application) messageSent(message string, destIp string) {
	app.eventLog.SentMessage(message, app.localIp, destIp)
}


func (app *Application) errorSending(message string, destIp string, err error) {
	app.eventLog.ErrorSending(message, app.localIp, destIp, err)
}


func (app *Application) getData() []byte {
	updates, err := app.pending.Poll(1, 1 * time.Millisecond)
	if updates == nil || err != nil {
		return nil
	}
	update := updates[0].(Update)

	update.cycles++
	// add back to pending if it didn't go over k cycles yet
	if (app.coin && rand.Float32() > app.dropProbability) || ( update.cycles < app.maxCycles ) {
		app.pending.Put(update)
	} else {
		app.eventLog.RemoveMessage(string(update.message), app.localIp)
	}

	return update.message
}

func (app *Application) processData(msg []byte) {
	msgAsString := string(msg)

	if !app.received.Exists(msgAsString) {
		// add to received, add to pending
		app.addMessage(msgAsString)
	} else{
		app.eventLog.ReceivedKnownMessage(msgAsString, app.localIp);
	}
}

func removeIp(hostsList []string, ip string) []string {
	index := find(hostsList, ip)
	return append(hostsList[:index], hostsList[index + 1:]...)
}

func (app *Application) addMessage(message string) bool {
	app.eventLog.ReceivedMessage(message, app.localIp)

	app.received.Add(message)
	return app.pending.Put(Update{[]byte(message), 0}) != nil
}

func find(list []string, thing string) int {
	for i, el := range list {
		if el == thing {
			return i
		}
	}
	return -1
}

func getIp() (string, error) {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		os.Stderr.WriteString("Oops: " + err.Error() + "\n")
		os.Exit(1)
	}

	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String(), nil
			}
		}
	}
	return "", errors.New("not found")
}
