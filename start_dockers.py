#!/usr/bin/env python
from functools import wraps
import netaddr
import signal
import time
import subprocess
import sys

n=100
gossip_port=5000
startSleep=0.1

class TimeoutError(Exception):
    pass

def timeout(seconds=60, error_message="timeout"):
    def decorator(func):
        def _handle_timeout(signum, frame):
            raise TimeoutError(error_message)

        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result

        return wraps(func)(wrapper)

    return decorator

def getContainersCount():
    count = "docker ps -q -f ancestor=gossip | wc -l"
    return int(subprocess.check_output(count, shell=True))

timeout(seconds=600, error_message="Not all containers were started in 360 seconds!")
def wait(n):
    c = 0
    while c !=n:
        c = getContainersCount()
        print("Currently running: %d containers" % c)
        time.sleep(startSleep)

def verifyNotYetRunning():
    if getContainersCount() > 0:
        print("Containers already running, please kill before trying to start again")
        sys.exit(1)

def start(n = 5):
    net = netaddr.IPNetwork('172.33.0.0/16')
    hosts = net[2:n+2]
    ips = ["%s" % v for v in hosts]
    all_ips = ' '.join(ips)

    port = 3000
    count = 0
    for ip in ips:
        print(ip)
        # only map port for first container
        if count == 0:
            # -v log:/app/log
            cmd = "docker run --rm --net gossip -e GOSSIP_PORT={0} -e KNOWN_HOSTS=\"{1}\" -p {3}:3000 --ip={2} gossip &"
        else:
            cmd = "docker run --rm --net gossip -e GOSSIP_PORT={0} -e KNOWN_HOSTS=\"{1}\" --ip={2} gossip &"

        fmt = cmd.format(gossip_port, all_ips, ip, port)

        subprocess.call(fmt, shell=True)
        port += 1
        count += 1
        time.sleep(1)

    print("Executed command to start %d containers" % n)
    wait(n)
    print("All containers started correctly!")

def main():
    verifyNotYetRunning()
    start(n)

if __name__ == "__main__":
    # stuff only to run when not called via 'import' here
    main()
